﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Entities
{
    public class Follow
    {
        public int Id { get; set; }

        [ForeignKey("Follower")]
        public int? FollowerId { get; set; }
        public User Follower { get; set; }


        [ForeignKey("Following")]
        public int? FollowingId { get; set; }
        public User Following { get; set; }

        public DateTime Date { get; set; }
    }
}