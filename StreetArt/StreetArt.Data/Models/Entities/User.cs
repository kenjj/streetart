﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Entities
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string Avatar { get; set; }
        public string Username { get; set; }
        public string Email{ get; set; }
        public string Password { get; set; }
        public DateTime? Birthday { get; set; }
        public string City { get; set; }
        public string UserFBId { get; set; }



        public string AboutTitle { get; set; }
        public string AboutText { get; set; }
        public string FbLink { get; set; }
        public string TwLink { get; set; }
        public string DribbleLink { get; set; }
        public string LinkdinLink { get; set; }
        public string VimeoLink{ get; set; }

    }
}