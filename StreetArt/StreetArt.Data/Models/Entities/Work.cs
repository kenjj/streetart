﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Entities
{
    public class Work
    {
        public Work()
        {
            Likes = new List<LikeToUser>();
            Views = new List<ViewToUser>();
            Shares = new List<ShareToUser>();
        }
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public User User { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }
        public string PosterImage { get; set; }

        public DateTime UploadedTime { get; set; }
        public DateTime ModifiedTime { get; set; }
        public bool Approved { get; set; }
        public bool Favourite { get; set; }
        public string defaultImage { get; set; }
        public string projectDescription { get; set; }

        public List<LikeToUser> Likes { get; set; }
        public List<ViewToUser> Views { get; set; }
        public List<ShareToUser> Shares { get; set; }
        public List<WorkToTag> Tags { get; set; }

        [ForeignKey("SubCategory")]
        public int? SubCategoryId { get; set; }
        public SubCategory SubCategory { get; set; }


    }
}