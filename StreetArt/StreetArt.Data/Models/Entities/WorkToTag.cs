﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Entities
{
    public class WorkToTag
    {
        public int Id { get; set; }

        [ForeignKey("Work")]
        public int? WorkId { get; set; }
        public Work Work { get; set; }


        [ForeignKey("Tag")]
        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}