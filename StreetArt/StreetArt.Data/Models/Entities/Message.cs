﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Entities
{
    public class Message
    {
        public int Id { get; set; }
        public string Content { get; set; }

        [ForeignKey("UserFrom")]
        public int? UserFromId { get; set; }
        public User UserFrom { get; set; }


        [ForeignKey("UserTo")]
        public int? UserToId { get; set; }
        public User UserTo { get; set; }

        public bool Seen { get; set; }
        public DateTime Date { get; set; }
    }
}