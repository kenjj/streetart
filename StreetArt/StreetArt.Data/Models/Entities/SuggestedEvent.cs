﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Entities
{
    public class SuggestedEvent
    {
        public int Id { get; set; }

        public string Author { get; set; }

        public string EventName { get; set; }
        public string Email { get; set; }
        public string Images { get; set; }
        public DateTime CreatedTime { get; set; }
        public bool Seen { get; set; }
    }
}