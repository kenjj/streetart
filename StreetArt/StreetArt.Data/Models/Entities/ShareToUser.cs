﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Entities
{
    public class ShareToUser
    {
        public int Id { get; set; }


        [ForeignKey("Work")]
        public int WorkId { get; set; }
        public Work Work { get; set; }

        public DateTime DateTime { get; set; }
    }
}