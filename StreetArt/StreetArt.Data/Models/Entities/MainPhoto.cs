﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Entities
{
    public class MainPhoto
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }

        public string mainImage { get; set; }

        [NotMapped]
        public HttpPostedFileBase defaultImage { get; set; }
    }
}