﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Entities
{
    public class SellContent
    {
        public int Id { get; set; }
        
        public string Content { get; set; }
    }
}