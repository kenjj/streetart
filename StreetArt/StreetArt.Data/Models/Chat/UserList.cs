﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Chat
{
    public class UserList
    {
        public Entities.User User { get; set; }
        public DateTime Date { get; set; }
    }
}