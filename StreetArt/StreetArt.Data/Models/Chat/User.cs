﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Data.Models.Chat
{
    public class User
    {
        public int UserId { get; set; }
        public string ConnectionId { get; set; }
    }
}