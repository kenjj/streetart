﻿using System;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using StreetArt.Data.Models.Chat;
using System.Security.Claims;
using System.Threading;
using System.Linq;
using StreetArt.Core.Services;
using System.Collections.Generic;
using StreetArt.Core.Models;
using StreetArt.Models;

namespace SignalRChat
{
    public class ChatHub : Hub
    {

        public void Connect(string token)
        {
            try
            {

                var resp = CryptoService.Decode(token);
                if (resp == "error")
                    throw new Exception("Not Valid JWT Token");

                var userId = Newtonsoft.Json.JsonConvert.DeserializeObject<CryptoData>(resp).userId;

                using (EFDbContext _db = new EFDbContext())
                {
                    var userExists = _db.Users.Any(q => q.Id == userId);
                    if (!userExists)
                        throw new Exception("Not User Like That");


                    Users.All.Add(new User
                    {
                        ConnectionId = Context.ConnectionId,
                        UserId = userId
                    });

                }


            }
            catch (Exception)
            {
                base.OnDisconnected(true);
            }

        }

        public void SendMessage(int userId, string content)
        {
            try
            {
                var user = Users.All.FirstOrDefault(q => q.ConnectionId == Context.ConnectionId);

                if (user == null)
                    throw new Exception();

                string encodedContent = HttpUtility.HtmlEncode(content);


                

                using (EFDbContext _db = new EFDbContext())
                 {
                        if (!_db.Users.Any(q => q.Id == userId))
                            throw new Exception();
                        var time = DateTime.Now;

                  

                        _db.Messages.Add(new StreetArt.Data.Models.Entities.Message
                        {
                            Content = encodedContent,
                            Date = time,
                            Seen = false,
                            UserFromId = user.UserId,
                            UserToId = userId
                        });
                        _db.SaveChanges();

                        
                        if(Users.All.Any(q => q.UserId == userId))
                        {
                            var OnlineUser =  Users.All.FirstOrDefault(q => q.UserId == userId);
                            
                            //if (!firstMessage)
                            //{
                            //    var toUser = _db.Users.FirstOrDefault(q => q.Id == userId);
                               
                            //    Clients.Client(OnlineUser.ConnectionId).newPersonMessage(toUser, encodedContent);
                                
                            //}
                            //else
                            //{

                           Clients.Client(OnlineUser.ConnectionId).newMessage(user.UserId, userId, encodedContent);
                            
                        }

                        
                    }




            }
            catch (Exception)
            {
                base.OnDisconnected(true);
            }
            
          
        

        }


        public void Seen(int userId)
        {
            try
            {

                var user = Users.All.FirstOrDefault(q => q.ConnectionId == Context.ConnectionId);

                if (user == null)
                    throw new Exception();


                using (EFDbContext _db = new EFDbContext())
                {

                    var messages = _db.Messages.Where(q => q.Seen == false && q.UserFromId == userId && q.UserToId == user.UserId).ToList();
                    foreach (var item in messages)
                    {
                        item.Seen = true;
                    }
                    _db.SaveChanges();
                }

            }
            catch (Exception)
            {

                
            }
        }

        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {

           
            Users.All.RemoveAll(q => q.ConnectionId == Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }
        public override Task OnReconnected()
        {
            
            return base.OnReconnected();
        }
    }
}