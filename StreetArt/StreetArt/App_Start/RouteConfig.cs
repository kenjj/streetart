﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace StreetArt
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            
            routes.MapRoute(
               name: "ChatUnreadMessages",
               url: "Chat/UnreadMessages",
               defaults: new { controller = "Chat", action = "UnreadMessages", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "ChatGetUser",
               url: "Chat/GetUser",
               defaults: new { controller = "Chat", action = "GetUser", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "ChatHistory",
               url: "Chat/History",
               defaults: new { controller = "Chat", action = "History", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "Chat",
               url: "Chat/{id}",
               defaults: new { controller = "Chat", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
