﻿using StreetArt.Core.Models;
using StreetArt.Models;
using StreetArt.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Helpers
{
    public static class AccountHelper
    {
        public static UserViewModel GetLoginedUser(){
            
            
                using (EFDbContext db = new EFDbContext())
                {
                    HttpContext context = HttpContext.Current;
                    var sess = context.Session[Config.UserLoginKey];
                    if (sess == null) return null;
                    int id = int.Parse(sess.ToString());
                    var user = db.Users.FirstOrDefault(q => q.Id == id);
                    var userModel = new UserViewModel()
                    {
                        Id = user.Id,
                        Username = user.Username,
                        FirstName = user.FirstName,
                        Avatar = user.Avatar
                    };
                    return userModel;
                }
                   
            
        }

        public static string GetGayideContent()
        {


            using (EFDbContext db = new EFDbContext())
            {
                var item = db.SellContents.FirstOrDefault();
                return item == null ? "" : item.Content;
              
            }


        }
    }
}