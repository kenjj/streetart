﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StreetArt.Data.Models.Entities;
using StreetArt.Models;
using StreetArt.Core.Services;
using System.IO;

namespace StreetArt.Controllers.Admin
{
    [Filters.AdminFilter]

    public class AdminSlidersController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: AdminSliders
        public ActionResult Index()
        {
            return View(db.Sliders.ToList());
        }

        // GET: AdminSliders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Sliders.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // GET: AdminSliders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminSliders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        
        public ActionResult Create(Slider slider)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(slider.Image) || slider.defaultImage == null)
                {
                    ModelState.AddModelError(string.Empty, "Error Adding Image.");
                    return View(slider);
                }

                slider.Image = slider.Image.Substring(slider.Image.IndexOf(',') + 1);

                var ImageObj = ImageServices.Base64ToImage(slider.Image);

                var PathUrl = Server.MapPath("~/Uploads/Sliders/");
                slider.Image = Guid.NewGuid() + ".png";
                string filePathImage = Path.Combine(PathUrl, slider.Image);
                ImageObj.Save(filePathImage, System.Drawing.Imaging.ImageFormat.Png);
                ImageObj.Dispose();


                string fileName = Guid.NewGuid() + slider.defaultImage.FileName;
                string filePath = Path.Combine(PathUrl, fileName);

                if (!ImageServices.saveImage(slider.defaultImage, filePath))
                {
                    ModelState.AddModelError(string.Empty, "Error Adding Image.");
                    return View(slider);
                }
                else
                {
                      slider.mainImage = fileName;
                      db.Sliders.Add(slider);
                      db.SaveChanges();
                    return RedirectToAction("Index");
                }

            }

            return View(slider);
        }

        // GET: AdminSliders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Sliders.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // POST: AdminSliders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        
        public ActionResult Edit(Slider slider)
        {
            if (ModelState.IsValid)
            {
                var currSlider = db.Sliders.FirstOrDefault(q => q.Id == slider.Id);
                var PathUrl = Server.MapPath("~/Uploads/Sliders/");

                if (slider.defaultImage != null)
                {
                    

                    string fileName = Guid.NewGuid() + slider.defaultImage.FileName;
                    string filePath = Path.Combine(PathUrl, fileName);
                    ImageServices.saveImage(slider.defaultImage, filePath);

                    
                    currSlider.mainImage = fileName;

                }

               
                slider.Image = slider.Image.Substring(slider.Image.IndexOf(',') + 1);

                var ImageObj = ImageServices.Base64ToImage(slider.Image);

                
                currSlider.Image = Guid.NewGuid() + ".png";

                string filePathImage = Path.Combine(PathUrl, currSlider.Image);
                ImageObj.Save(filePathImage, System.Drawing.Imaging.ImageFormat.Png);
                ImageObj.Dispose();

                
                


                currSlider.Link = slider.Link;


                
                db.SaveChanges();
                return RedirectToAction("Index");
                
              
            }
            return View(slider);
        }

        // GET: AdminSliders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Sliders.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // POST: AdminSliders/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public ActionResult DeleteConfirmed(int id)
        {
            Slider slider = db.Sliders.Find(id);

            var PathUrl = Server.MapPath("~/Uploads/Events/");
            if (!string.IsNullOrEmpty(slider.mainImage))
            {
                
                string filePathMain = Path.Combine(PathUrl, slider.mainImage);
                FileInfo fileMain = new FileInfo(filePathMain);
                if (fileMain.Exists)
                {
                    fileMain.Delete();
                }

            }



            if (string.IsNullOrEmpty(slider.Image))
            {
                string filePath = Path.Combine(PathUrl, slider.Image);
                FileInfo file = new FileInfo(filePath);
                if (file.Exists)
                {
                    file.Delete();
                }
            }
            


            db.Sliders.Remove(slider);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
