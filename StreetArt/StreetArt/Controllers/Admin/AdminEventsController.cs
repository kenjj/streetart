﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StreetArt.Data.Models.Entities;
using StreetArt.Models;
using System.IO;
using System.Web.Hosting;
using StreetArt.Core.Services;

namespace StreetArt.Controllers.Admin
{
    [Filters.AdminFilter]
    public class AdminEventsController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: AdminEvents
        public ActionResult Index()
        {
            return View(db.Events.OrderByDescending(q=>q.CreatedDate).ToList());
        }

        // GET: AdminEvents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // GET: AdminEvents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminEvents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        
        public ActionResult Create(Event @event)
        {
            if (ModelState.IsValid)
            {
                if (@event._Image == null)
                {
                    ModelState.AddModelError(string.Empty, "Error Adding Image.");
                    return View(@event);
                }

                string fileName = Guid.NewGuid() + @event._Image.FileName;
                var PathUrl = Server.MapPath("~/Uploads/Events/");

                string filePath = Path.Combine(PathUrl, fileName);

                if (!ImageServices.saveImage(@event._Image, filePath))
                {
                    ModelState.AddModelError(string.Empty, "Error Adding Image.");
                    return View(@event);
                }else
                {

                    @event.Image = fileName;
                    @event.CreatedDate = DateTime.Now;
                    @event.ModifiedDate = DateTime.Now;
                    db.Events.Add(@event);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }

               
            }

            return View(@event);
        }

        // GET: AdminEvents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: AdminEvents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        
        public ActionResult Edit(Event @event)
        {
            if (ModelState.IsValid)
            {
                
                var ev = db.Events.FirstOrDefault(q => q.Id == @event.Id);
                if (@event._Image != null)
                {
                    string fileName = Guid.NewGuid() + @event._Image.FileName;

                    var PathUrl = Server.MapPath("~/Uploads/Events/");

                    string filePath = Path.Combine(PathUrl, fileName);


                    if (ImageServices.saveImage(@event._Image, filePath))
                    {
                        
                        string filePathOld = Path.Combine(PathUrl, ev.Image);
                        FileInfo file = new FileInfo(filePathOld);
                        if (file.Exists)
                        {
                            file.Delete();
                        }
                        ev.Image = fileName;
                    }
                }
                ev.Title = @event.Title;
                ev.StartDate = @event.StartDate;
                ev.Location = @event.Location;
                ev.Description = @event.Description;
                ev.Time = @event.Time;
                ev.Latitude = @event.Latitude;
                ev.Longitude = @event.Longitude;
                ev.ModifiedDate = DateTime.Now;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(@event);
        }

        // GET: AdminEvents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: AdminEvents/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public ActionResult DeleteConfirmed(int id)
        {
            Event @event = db.Events.Find(id);
            var PathUrl = Server.MapPath("~/Uploads/Events/");
            string filePath = Path.Combine(PathUrl, @event.Image);
            FileInfo file = new FileInfo(filePath);
            if (file.Exists)
            {
                file.Delete();
            }
            db.Events.Remove(@event);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
