﻿using StreetArt.Core.Models;
using StreetArt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StreetArt.Controllers.Admin
{
    public class AdminController : Controller
    {
        EFDbContext db = new EFDbContext();
        public ActionResult Index()
        {
            if (StreetArt.Core.Helpers.AuthHelper.AdminLoggedIn)
                return RedirectToAction("Index", "AdminEvents");
            return View();
        }

        [HttpPost]
        public ActionResult Index(string UserName, string Password)
        {

            var Admin = db.AdminUsers.FirstOrDefault(a => a.Username == UserName && a.Password == Password);

            if (Admin == null)
            {
                ViewBag.Error = "მომხმარებლის სახელი ან პაროლი არასწორია!";
            }
            else
            {
                Session[Core.Models.Config.AdminLoginKey] = true;
                if (Admin.MainAdmin)
                {
                    Session[Config.MainAdminLoginKey] = true;

                }
                return RedirectToAction("Index", "AdminEvents");
            }
            return View();
        }


        public ActionResult logout()
        {
            Session[Core.Models.Config.AdminLoginKey] = null;
            Session[Core.Models.Config.MainAdminLoginKey] = null;

            return RedirectToAction("Index", "Admin");
        }

    }
}