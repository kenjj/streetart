﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StreetArt.Data.Models.Entities;
using StreetArt.Models;

namespace StreetArt.Controllers.Admin
{
    [Filters.AdminFilter]
    public class AdminSellContentsController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: AdminSellContents
        public ActionResult Index()
        {
            var SellContent = db.SellContents.FirstOrDefault();
            if (SellContent == null)
                return RedirectToAction("create");
            else
            {
                return RedirectToAction("edit", new { id = SellContent.Id });
            }
        }

        // GET: AdminSellContents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SellContent sellContent = db.SellContents.Find(id);
            if (sellContent == null)
            {
                return HttpNotFound();
            }
            return View(sellContent);
        }

        // GET: AdminSellContents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminSellContents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        
        public ActionResult Create([Bind(Include = "Id,Content")] SellContent sellContent)
        {
            if (ModelState.IsValid)
            {
                db.SellContents.Add(sellContent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sellContent);
        }

        // GET: AdminSellContents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SellContent sellContent = db.SellContents.Find(id);
            if (sellContent == null)
            {
                return HttpNotFound();
            }
            return View(sellContent);
        }

        // POST: AdminSellContents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,Content")] SellContent sellContent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sellContent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sellContent);
        }

        // GET: AdminSellContents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SellContent sellContent = db.SellContents.Find(id);
            if (sellContent == null)
            {
                return HttpNotFound();
            }
            return View(sellContent);
        }

        // POST: AdminSellContents/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            SellContent sellContent = db.SellContents.Find(id);
            db.SellContents.Remove(sellContent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
