﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StreetArt.Data.Models.Entities;
using StreetArt.Models;

namespace StreetArt.Controllers.Admin
{
    [Filters.AdminFilter]

    public class AdminSuggestedEventsController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: AdminSuggestedEvents
        public ActionResult Index()
        {
            return View(db.SuggestedEvents.ToList());
        }

        // GET: AdminSuggestedEvents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuggestedEvent suggestedEvent = db.SuggestedEvents.Find(id);
            if (suggestedEvent == null)
            {
                return HttpNotFound();
            }
            return View(suggestedEvent);
        }
       

        // GET: AdminSuggestedEvents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuggestedEvent suggestedEvent = db.SuggestedEvents.Find(id);
            if (suggestedEvent == null)
            {
                return HttpNotFound();
            }
            return View(suggestedEvent);
        }

        // POST: AdminSuggestedEvents/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public ActionResult DeleteConfirmed(int id)
        {
            SuggestedEvent suggestedEvent = db.SuggestedEvents.Find(id);
            db.SuggestedEvents.Remove(suggestedEvent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
