﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StreetArt.Data.Models.Entities;
using StreetArt.Models;
using StreetArt.Core.Services;
using System.IO;

namespace StreetArt.Controllers.Admin
{
    [Filters.AdminFilter]
    public class AdminMainPhotosController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: AdminMainPhotos
        public ActionResult Index()
        {
            var photos = db.MainPhotoes.FirstOrDefault();
            if (photos == null)
                return RedirectToAction("create");
            else
            {
                return RedirectToAction("edit", new { id = photos.Id });
            }
        }

        // GET: AdminMainPhotos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MainPhoto mainPhoto = db.MainPhotoes.Find(id);
            if (mainPhoto == null)
            {
                return HttpNotFound();
            }
            return View(mainPhoto);
        }

        // GET: AdminMainPhotos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminMainPhotos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        
        public ActionResult Create(MainPhoto mainPhoto)
        {
            if (string.IsNullOrEmpty(mainPhoto.Image) || mainPhoto.defaultImage == null)
            {
                ModelState.AddModelError(string.Empty, "Error Adding Image.");
                return View(mainPhoto);
            }

            mainPhoto.Image = mainPhoto.Image.Substring(mainPhoto.Image.IndexOf(',') + 1);

            var ImageObj = ImageServices.Base64ToImage(mainPhoto.Image);

            var PathUrl = Server.MapPath("~/Uploads/Sliders/");
            mainPhoto.Image = Guid.NewGuid() + ".png";
            string filePathImage = Path.Combine(PathUrl, mainPhoto.Image);
            ImageObj.Save(filePathImage, System.Drawing.Imaging.ImageFormat.Png);
            ImageObj.Dispose();


            string fileName = Guid.NewGuid() + mainPhoto.defaultImage.FileName;
            string filePath = Path.Combine(PathUrl, fileName);

            if (!ImageServices.saveImage(mainPhoto.defaultImage, filePath))
            {
                ModelState.AddModelError(string.Empty, "Error Adding Image.");
                return View(mainPhoto);
            }
            else
            {
                mainPhoto.mainImage = fileName;
                db.MainPhotoes.Add(mainPhoto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        // GET: AdminMainPhotos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MainPhoto mainPhoto = db.MainPhotoes.Find(id);
            if (mainPhoto == null)
            {
                return HttpNotFound();
            }
            return View(mainPhoto);
        }

        // POST: AdminMainPhotos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        
        public ActionResult Edit(MainPhoto mainPhoto)
        {
            if (ModelState.IsValid)
            {
                var currSlider = db.MainPhotoes.FirstOrDefault(q => q.Id == mainPhoto.Id);
                var PathUrl = Server.MapPath("~/Uploads/Sliders/");

                if (mainPhoto.defaultImage != null)
                {


                    string fileName = Guid.NewGuid() + mainPhoto.defaultImage.FileName;
                    string filePath = Path.Combine(PathUrl, fileName);
                    ImageServices.saveImage(mainPhoto.defaultImage, filePath);


                    currSlider.mainImage = fileName;

                }


                mainPhoto.Image = mainPhoto.Image.Substring(mainPhoto.Image.IndexOf(',') + 1);

                var ImageObj = ImageServices.Base64ToImage(mainPhoto.Image);


                currSlider.Image = Guid.NewGuid() + ".png";

                string filePathImage = Path.Combine(PathUrl, currSlider.Image);
                ImageObj.Save(filePathImage, System.Drawing.Imaging.ImageFormat.Png);
                ImageObj.Dispose();





                currSlider.Link = mainPhoto.Link;


                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mainPhoto);
        }

        // GET: AdminMainPhotos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MainPhoto mainPhoto = db.MainPhotoes.Find(id);
            if (mainPhoto == null)
            {
                return HttpNotFound();
            }
            return View(mainPhoto);
        }

        // POST: AdminMainPhotos/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public ActionResult DeleteConfirmed(int id)
        {
            MainPhoto mainPhoto = db.MainPhotoes.Find(id);
            db.MainPhotoes.Remove(mainPhoto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
