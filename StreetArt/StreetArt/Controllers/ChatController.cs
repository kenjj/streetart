﻿using StreetArt.Core.Services;
using StreetArt.Data.Models.Chat;
using StreetArt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StreetArt.Controllers
{

    [Filters.UserFilter]
    public class ChatController : Controller
    {
        private EFDbContext _db = new EFDbContext();

        public ActionResult Index(int? id)
        {
            
            var userModel = Helpers.AccountHelper.GetLoginedUser();

            var payload = new Dictionary<string, object>()
            {
                { "userId", userModel.Id }
            };
            var data = CryptoService.Encode(payload);
            ViewBag.data = data;


            var UserChatList = new List<UserListShowHelper>();

            var queryTo = _db.Messages.Where(u => u.UserToId == userModel.Id).GroupBy(b => b.UserFromId);

            if (queryTo.Any())
            {
                var res = queryTo.Select(u => new UserListShowHelper
                {
                    Id = u.FirstOrDefault().Id,
                    UserId = u.FirstOrDefault().UserFromId,
                    Date = u.FirstOrDefault().Date
                }).OrderByDescending(q => q.Id).ToList();


                UserChatList.AddRange(res);
            }

            var queryFrom = _db.Messages.Where(u => u.UserFromId == userModel.Id).GroupBy(b => b.UserToId);

            if (queryFrom.Any())
            {
                var res = queryFrom.Select(u => new UserListShowHelper
                {
                    Id = u.FirstOrDefault().Id,
                    UserId = u.FirstOrDefault().UserToId,
                    Date = u.FirstOrDefault().Date
                }).OrderByDescending(q => q.Id).ToList();


                UserChatList.AddRange(res);

            }

            var userDinstictQuery = UserChatList.GroupBy(q => q.UserId);

            

            if (userDinstictQuery.Any())
            {
                UserChatList  =  userDinstictQuery.Select(o => new UserListShowHelper
                {
                    UserId =  o.FirstOrDefault().UserId,
                    Date = o.FirstOrDefault().Date
                }).ToList();
            }

            var userList = new List<UserList>();

            var users = _db.Users;

            foreach (var item in UserChatList)
            {
                userList.Add(new UserList
                {
                    User = users.FirstOrDefault(q=>q.Id == item.UserId),
                    Date = item.Date
                });
            }


            if (id != null)
            {
                var firstUser =  userList.FirstOrDefault(q => q.User.Id == id);

                if (firstUser == null)
                {
                    if(_db.Users.Any(q => q.Id == id))
                    {
                        var newUser = _db.Users.Select(q => new UserList
                        {
                            Date = DateTime.Now,
                            User = q
                        }).FirstOrDefault(q => q.User.Id == id);

                        if (newUser != null) userList.Insert(0, newUser);

                    }
                }else
                {
                   var index = userList.IndexOf(firstUser);
                    if(index != 0)
                    {
                        userList.Remove(firstUser);
                        userList.Insert(0, firstUser);
                    }
                    
                }

            }

            ViewBag.userList = userList;


            return View();
        }
    

        public JsonResult History(int? id)
        {
            try
            {


                var userExists = _db.Users.Any(q => q.Id == id);

                if (!userExists)
                    throw new Exception();

                
                   
                var me = Helpers.AccountHelper.GetLoginedUser();

                var Messages = _db.Messages.Where(u => (u.UserToId == me.Id && u.UserFromId == id) || (u.UserFromId == me.Id && u.UserToId == id))
                                .Select(q=> new
                                {
                                    q.Id,
                                    q.Content,
                                    me = q.UserToId == me.Id ? true : false
                                })
                                .OrderByDescending(q=>q.Id).Take(20).ToList();

             

                return Json(new
                {
                    Error = false,
                    Messages = Messages.OrderBy(q=>q.Id)
                });
            }
            catch (Exception)
            {
                return Json(new
                {
                    Error = true
                });
            }
            
        }

        [HttpPost]
        public JsonResult GetUser(int id)
        {
            try
            {

                var user = _db.Users.FirstOrDefault(q => q.Id == id);

                if (user == null)
                    throw new Exception();

                var userModel = Helpers.AccountHelper.GetLoginedUser();

                var messagesCount = _db.Messages.Where(q => q.UserFromId == user.Id && q.UserToId == userModel.Id && q.Seen == false).Count();

                dynamic dynObject = new {
                    Id = user.Id,
                    Avatar = user.Avatar,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Date = DateTime.Now.ToShortDateString(),
                    MessageCount = messagesCount
                };

                return Json(new
                {
                    Error = false,
                    User = dynObject
                });
            }
            catch (Exception)
            {
                return Json(new
                {
                    Error = true
                });
            }
        }

        [HttpPost]
        public JsonResult UnreadMessages(int id)
        {
            try
            {
                var count = 0;
                var queryFrom = _db.Messages
                    .Where(u => u.UserToId == id && u.Seen == false)
                    .GroupBy(b => b.UserFromId);

                if (queryFrom.Any())
                {
                    count = queryFrom.Select(u => new UserListShowHelper
                    {
                        Id = u.FirstOrDefault().Id,
                        UserId = u.FirstOrDefault().UserFromId,
                        Date = u.FirstOrDefault().Date
                    }).OrderBy(q => q.Id).Count();
                }


                return Json(new
                {
                    Count = count
                });
            }

            catch (Exception)
            {
                return Json(new
                {
                    Error = true
                });
            }

        }
    }
}