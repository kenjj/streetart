﻿using StreetArt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StreetArt.Controllers
{
    public class EventsController : Controller
    {
        // GET: Events
        private EFDbContext _db = new EFDbContext();
        public ActionResult Index(string timeSort = "desc",string qsearch = "")
        {
            ViewBag.timeSort = timeSort;
            ViewBag.qsearch = qsearch;

            IQueryable<StreetArt.Data.Models.Entities.Event> events = _db.Events;

            if (!string.IsNullOrEmpty(qsearch))
                events = events.Where(q => q.Title.Contains(qsearch) || q.Description.Contains(qsearch));

            if (timeSort == "desc")
                events = events.OrderByDescending(q => q.CreatedDate);
            else
                events = events.OrderBy(q => q.CreatedDate);

           
            


            return View(events.ToList());
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}