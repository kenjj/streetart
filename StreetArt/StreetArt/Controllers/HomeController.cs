﻿using Newtonsoft.Json;
using StreetArt.Core.Services;
using StreetArt.Data.Models.Entities;
using StreetArt.Models;
using StreetArt.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace StreetArt.Controllers
{
    public class HomeController : Controller
    {
        private EFDbContext _db = new EFDbContext();
        public ActionResult Index()
        {
            var model = new HomeViewModel();
            model.Events = _db.Events.OrderByDescending(q => q.CreatedDate).Take(6).ToList();
            model.Markers = _db.Events.Select(o => new LngLat
            {
                Lat = o.Latitude,
                Lng = o.Longitude
            }).ToList();
            model.UserLikedPosts = new List<int>();

            bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();


            if (UserLogedIn)
            {
                var userModel = StreetArt.Helpers.AccountHelper.GetLoginedUser();
                model.UserLikedPosts = _db.LikesToUsers.Where(q => q.UserId == userModel.Id).Select(q => q.WorkId).ToList();
            }
           

            

            model.Sliders = _db.Sliders.ToList();
            model.MainPhoto = _db.MainPhotoes.FirstOrDefault();
           
            model.FavouriteWorks = _db.Works.Select(o => new WorkViewModel
            {
                Id = o.Id,
                PosterImage = o.PosterImage,
                UserId = o.UserId,
                Likes = o.Likes.Count(),
                Shares = o.Shares.Count(),
                Views =o.Views.Count(),
                Title = o.Title,
                Favourite = o.Favourite,
                User = o.User,
                UploadedTime = o.UploadedTime
            }).Where(q => q.Favourite == true).OrderByDescending(q => q.Likes).Take(5).ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult SuggestedEvent(string uname, string eventName, string email, List<HttpPostedFileBase> images)
        {
            try
            {
                SuggestedEvent newEvent = new SuggestedEvent();
                newEvent.Author = uname;
                newEvent.CreatedTime = DateTime.Now;
                newEvent.Email = email;
                newEvent.EventName = eventName;
                newEvent.Images = "";
                if(images.Count != 0)
                {
                    var PathUrl = Server.MapPath("~/Uploads/SuggestedEvents/");
                    var fileNames = new List<string>();
                    foreach (var item in images)
                    {
                        string fileName = Guid.NewGuid() + item.FileName;
                        string filePath = Path.Combine(PathUrl, fileName);
                        ImageServices.saveImage(item, filePath);
                        fileNames.Add(fileName);

                    }
                   newEvent.Images =  JsonConvert.SerializeObject(fileNames);

                }
               
                newEvent.Seen = false;

                
                _db.SuggestedEvents.Add(newEvent);
                _db.SaveChanges();
                TempData["successUploaded"] = true;
                return RedirectToAction("index");
            }
            catch (Exception)
            {
                TempData["successUploaded"] = false;
                return RedirectToAction("index");
                
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}