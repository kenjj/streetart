﻿using Facebook;
using StreetArt.Core.Models;
using StreetArt.Core.Services;
using StreetArt.Data.Models.Entities;
using StreetArt.Models;
using StreetArt.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;

namespace StreetArt.Controllers
{
    public class AccountController : Controller
    {
      

        EFDbContext db = new EFDbContext();
        // GET: Account
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {

                var ExistingUserName =
                    db.Users
                    .Where(u => u.Username == model.UserName)
                    .FirstOrDefault();
                if (ExistingUserName != null)
                {
                    ViewBag.Error = "მომხმარებლის სახელი დაკავებულია";
                    return View();
                }



                User user = new User()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Username = model.UserName,
                    City = model.City,
                    Email = model.Email,
                    Password = Core.Helpers.Crypto.sha256_hash(model.Password),
                    Birthday = new DateTime(model.Year, model.Month, model.Day),
                    Avatar = "avatar.png",
                    UserFBId = "",
                };
                db.Users.Add(user);
                db.SaveChanges();
                TempData["Justlogined"] = true;
                return RedirectToAction("Login");
            }
            return View();
        }



        [HttpGet]
        public ActionResult Users()
        {
            var users = db.Works.GroupBy(q => q.UserId)
                .Select(grp => grp.FirstOrDefault().User)
                .ToList();
            return View(users);
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (TempData["Justlogined"] != null)
            {
                ViewBag.JustLogined = "თქვენ წარმატებით დარეგისტრირდით. გთხოვთ გაიაროთ ავტორიზაცია";
            }

            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                string password = Core.Helpers.Crypto.sha256_hash(model.Password);
                var user = db.Users
                    .FirstOrDefault(u => u.Username == model.UserName && u.Password == password);
                if (user == null)
                {
                    ViewBag.Error = "მომხმარებელი ან პაროლი არასწორია";
                    return View();
                }
                else
                {
                    Session[Config.UserLoginKey] = user.Id;



                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
        }


        [HttpPost]
        public JsonResult Follow(int userid)
        {
            try
            {

                bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();


                if (!UserLogedIn)
                    throw new Exception();
                    
                var userModel = Helpers.AccountHelper.GetLoginedUser();


                if (userModel.Id == userid)
                    throw new Exception();

                var follow = db.Follows.Any(q => q.FollowerId == userModel.Id && q.FollowingId == userid);

                if (follow)
                    throw new Exception();
                var now = DateTime.Now;

                db.Follows.Add(new Data.Models.Entities.Follow
                {
                    FollowerId = userModel.Id,
                    FollowingId = userid,
                    Date = now
                });
                db.SaveChanges();
                




                return Json(new
                {
                    error = false
                });
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                });
            }
        }

        [HttpGet]
        public ActionResult Details()
        {
            bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();


            if (!UserLogedIn)
            {
                return RedirectToAction("Login", "Account");
            }
            var userModel = StreetArt.Helpers.AccountHelper.GetLoginedUser();

            User user = db.Users.FirstOrDefault(q => q.Id == userModel.Id);

            return View(user);
        }

        [HttpPost]
        public ActionResult Details(HttpPostedFileBase Photo, DetailsViewModel model)
        {

            bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();

            
            if (!UserLogedIn)
            {
                return RedirectToAction("Login", "Account");
            }

            var userModel = StreetArt.Helpers.AccountHelper.GetLoginedUser();

            User user = db.Users.FirstOrDefault(q => q.Id == userModel.Id);

            if (Photo != null)
            {
                var fileName = "";

                if (Path.GetExtension(Photo.FileName).ToLower() == ".jpg" ||
                    Path.GetExtension(Photo.FileName).ToLower() == ".png" ||
                    Path.GetExtension(Photo.FileName).ToLower() == ".gif" ||
                    Path.GetExtension(Photo.FileName).ToLower() == ".jpeg")       //ვამოწმებთ ასატვირთი ფაილის ფორმატს
                {
                    var size = Photo.ContentLength;
                    double b = size / (1024 * 1024);
                    if (b > 2)
                        ViewBag.Error = "ფოტოს ზომა არ უნდა იყოს 2 მეგაბაიტზე მეტი.";
                    fileName = Guid.NewGuid() + Path.GetFileName(Photo.FileName);
                    var path = Path.Combine(Server.MapPath("~/Images/UserPhotos"), fileName);
                    Photo.SaveAs(path);


                    user.Avatar = fileName;

                    db.SaveChanges();
                    ViewBag.Success = "ფოტო წარმატებით შეიცვალა";


                    return View(user);
                }

                ViewBag.Error = "ფოტოს ფორმატი არასწორია";
                return View(user);
            }
            else
            {
                user.AboutTitle = model.AboutTitle;
                user.AboutText = model.AboutText;
                user.FbLink = model.FbLink;
                user.TwLink = model.TwLink;
                user.DribbleLink = model.DribbleLink;
                user.LinkdinLink = model.LinkdinLink;
                user.VimeoLink = model.VimeoLink;
                db.SaveChanges();
                return View(user);
            }
        }

        [HttpPost]
        public JsonResult FBLogin(string accessToken)
        {
            try
            {
                var client = new FacebookClient(accessToken);
                dynamic result = client.Get("me", new { fields = "name,id" });

                if (result == null) new System.ArgumentException("Not Access To fb account/Invalid Token");

                FbUser fbUsr = new FbUser()
                {
                    Id = result.id,
                    fullname = result.name
                };

                var rs = db.Users.FirstOrDefault(q => q.UserFBId == fbUsr.Id);


                string registered = String.Empty;

                if (rs != null)
                {
                    Session[Config.UserLoginKey] = rs.Id;
                }
                else
                {

                    var ind = fbUsr.fullname.IndexOf(" ");
                    var firsname = string.Empty;
                    var lastname = string.Empty;
                    if (ind > -1)
                    {
                        firsname = fbUsr.fullname.Substring(0, ind);
                        lastname = fbUsr.fullname.Substring(ind + 1);
                    }
                    else
                    {
                        firsname = fbUsr.fullname;
                    }
                    User user = new User()
                    {

                        FirstName = firsname,
                        LastName = lastname,
                        Username = "",
                        City = "",
                        Email = "",
                        Password = "",
                        Birthday = DateTime.Now,
                        Avatar = "avatar.png",
                        UserFBId = fbUsr.Id,
                        AboutTitle = "ჩემს შესახებ", 
                        AboutText = "აღწერა არაა"
                    };
                    db.Users.Add(user);
                    db.SaveChanges();

                    Session[Config.UserLoginKey] = user.Id;



                }

                return Json(new
                {
                    error = false,
                });
            }
            catch (Exception)
            {

                return Json(new
                {
                    error = true,
                });
            }
        }

        public ActionResult AddWork()
        {
            bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();
            if (!UserLogedIn)
            {
                return RedirectToAction("Login", "Account");
            }


            ViewBag.Tags = db.Tags.ToList();
            ViewBag.Categories = db.Categories.Include(q=>q.SubCategories).ToList();

            return View();
        }

        [HttpPost]
        public ActionResult AddWork(PortFolioViewModel model)
        {

            var tags = db.Tags.ToList();
            var categories = db.Categories.Include(q => q.SubCategories).ToList();
          
            bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();
            if (!UserLogedIn)
            {
                return RedirectToAction("Login", "Account");
            }
            try
            {
                if (ModelState.IsValid)
                {
                   
                    model.PosterImage = model.PosterImage.Substring(model.PosterImage.IndexOf(',') + 1);

                    var ImageObj = ImageServices.Base64ToImage(model.PosterImage);

                    var PathUrl = Server.MapPath("~/Uploads/UserWorks/");
                    model.PosterImage = Guid.NewGuid() + ".png";
                    string filePathImage = Path.Combine(PathUrl, model.PosterImage);
                    ImageObj.Save(filePathImage, System.Drawing.Imaging.ImageFormat.Png);
                    ImageObj.Dispose();


                    string fileName = Guid.NewGuid() + model.defaultImage.FileName;
                    string filePath = Path.Combine(PathUrl, fileName);

                    if (!ImageServices.saveImage(model.defaultImage, filePath))
                    {
                        ViewBag.Error = "ფოტო ვერ დაემატა!";
                        return View();
                    }
                    else
                    {
                        var work = new Work()
                        {
                            defaultImage = fileName,
                            Description = model.Discription,
                            Title = model.Title,
                            PosterImage = model.PosterImage,
                            UploadedTime = DateTime.Now,
                            Favourite = false,
                            ModifiedTime = DateTime.Now,
                            Approved = false,
                            SubCategoryId = model.subcategory,
                            projectDescription = model.projectDescription,
                            UserId = Helpers.AccountHelper.GetLoginedUser().Id
                        };
                        db.Works.Add(work);

                         db.SaveChanges();


                        if(model.tags!= null)
                        {
                            foreach (var item in model.tags)
                            {
                                db.WorkToTags.Add(new WorkToTag
                                {
                                    TagId = item,
                                    WorkId = work.Id
                                });

                            }


                            db.SaveChanges();
                        }
                        ViewBag.Success = "ნაშრომი წარმატებით დაემატა";
                    }
                }
                else
                {
                    ViewBag.Error = "სათაური და ტექსტი სავალდებულო ველებია";
                }
            }
            catch (Exception)
            {

            }  

            ViewBag.Categories = categories;
            ViewBag.Tags = tags;
            return View();
        }


        public ActionResult EditWork(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Work work = db.Works.Find(id);
            if (work == null)
            {
                return HttpNotFound();
            }
            return View(work);
        }

        [HttpPost]
        public ActionResult EditWork(PortFolioViewModel model)
        {
            bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();
            if (!UserLogedIn)
            {
                return RedirectToAction("Login", "Account");
            }

            if (ModelState.IsValid)
            {

                var thisWork = db.Works.Find(model.Id);
                var PathUrl = Server.MapPath("~/Uploads/UserWorks/");

                if (thisWork.UserId != Helpers.AccountHelper.GetLoginedUser().Id)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);



                if (model.PosterImage != thisWork.PosterImage && !String.IsNullOrEmpty(model.PosterImage))
                {
                    model.PosterImage = model.PosterImage.Substring(model.PosterImage.IndexOf(',') + 1);
                    var ImageObj = ImageServices.Base64ToImage(model.PosterImage);
                    model.PosterImage = Guid.NewGuid() + ".png";
                    string filePathImage = Path.Combine(PathUrl, model.PosterImage);
                    ImageObj.Save(filePathImage, System.Drawing.Imaging.ImageFormat.Png);
                    ImageObj.Dispose();


                    thisWork.PosterImage = model.PosterImage;
                }


                if (model.defaultImage.FileName != thisWork.defaultImage && !String.IsNullOrEmpty(model.defaultImage.FileName))
                {
                    string fileName = Guid.NewGuid() + model.defaultImage.FileName;
                    string filePath = Path.Combine(PathUrl, fileName);
                    if (!ImageServices.saveImage(model.defaultImage, filePath))
                    {
                        ViewBag.Error = "ფოტო ვერ დაემატა!";
                        return View();
                    }

                    thisWork.defaultImage = fileName;
                }




                if (model.Title != thisWork.Title && !String.IsNullOrEmpty(thisWork.Title))
                {
                    thisWork.Title = model.Title;
                }

                db.SaveChanges();
                ViewBag.Success = "ნაშრომი წარმატებით დაემატა";
            }

            else
            {
                ViewBag.Error = "სათაური და ტექსტი სავალდებულო ველებია";
            }

            return View();
        }


        public ActionResult Profile(int? id)
        {

            try
            {
                


                if (id == null) throw new Exception();

                using (EFDbContext _db = new EFDbContext())
                {
                    var user = _db.Users.FirstOrDefault(q => q.Id == id);
                    if (user == null) throw new Exception();


                    var mainModel = new WorkViewModelPr();


                    var model = _db.Works.Select(o => new WorkViewModel
                    {
                        Id = o.Id,
                        PosterImage = o.PosterImage,
                        Likes = o.Likes.Count(),
                        Shares = o.Shares.Count(),
                        Views = o.Views.Count(),
                        UserId = o.UserId,
                        Approved = o.Approved,
                        Title = o.Title,
                        Favourite = o.Favourite,
                        User = o.User,
                        UploadedTime = o.UploadedTime
                    }).Where(q => q.Approved == true && q.UserId == user.Id).OrderBy(q => q.UploadedTime).ToList();





                    mainModel.UserLikedPosts = new List<int>();
                    mainModel.UserProfile = user;

                    bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();
                    if (UserLogedIn)
                    {
                        var userModel = StreetArt.Helpers.AccountHelper.GetLoginedUser();
                        mainModel.UserLikedPosts = _db.LikesToUsers.Where(q => q.UserId == userModel.Id).Select(q => q.WorkId).ToList();
                    }

                    mainModel.Works = model;


                    var userPosts = _db.Works.Where(q => q.UserId == user.Id);


                    ViewBag.UserPostLikes = userPosts.Select(q => q.Likes.Count).Any() ? userPosts.Select(q => q.Likes.Count).Sum() : 0 ;
                    ViewBag.UserPostViews = userPosts.Select(q => q.Views.Count).Any() ? userPosts.Select(q => q.Views.Count).Sum() : 0;
                    ViewBag.Followers = _db.Follows.Where(q => q.FollowingId == user.Id).Count();
                    ViewBag.Following = _db.Follows.Where(q => q.FollowerId == user.Id).Count();


                    return View(mainModel);

                }
            }
            catch (Exception)
            {
                return RedirectToAction("index","Works");
            }
        }





        public ActionResult Follows(int? id)
        {
            try
            {



                if (id == null) throw new Exception();

                using (EFDbContext _db = new EFDbContext())
                {
                    var user = _db.Users.FirstOrDefault(q => q.Id == id);
                    if (user == null) throw new Exception();


                    ViewBag.user = user;

                    var model = _db.Follows.Include(q=>q.Follower).Where(q => q.FollowingId == id).ToList();



                    var userPosts = _db.Works.Where(q => q.UserId == user.Id);

                    ViewBag.UserPostLikes = userPosts.Select(q => q.Likes.Count).Sum();
                    ViewBag.UserPostViews = userPosts.Select(q => q.Views.Count).Sum();
                    ViewBag.Followers = _db.Follows.Where(q => q.FollowingId == user.Id).Count();
                    ViewBag.Following = _db.Follows.Where(q => q.FollowerId == user.Id).Count();


                    return View(model);

                }
            }
            catch (Exception)
            {
                return RedirectToAction("index", "Works");
            }
        }



        public ActionResult Follower(int? id)
        {
            try
            {



                if (id == null) throw new Exception();

                using (EFDbContext _db = new EFDbContext())
                {
                    var user = _db.Users.FirstOrDefault(q => q.Id == id);
                    if (user == null) throw new Exception();


                    ViewBag.user = user;

                    var model = _db.Follows.Include(q => q.Following).Where(q => q.FollowerId == id).ToList();



                    var userPosts = _db.Works.Where(q => q.UserId == user.Id);

                    ViewBag.UserPostLikes = userPosts.Select(q => q.Likes.Count).Sum();
                    ViewBag.UserPostViews = userPosts.Select(q => q.Views.Count).Sum();
                    ViewBag.Followers = _db.Follows.Where(q => q.FollowingId == user.Id).Count();
                    ViewBag.Following = _db.Follows.Where(q => q.FollowerId == user.Id).Count();


                    return View(model);

                }
            }
            catch (Exception)
            {
                return RedirectToAction("index", "Works");
            }
        }


        [HttpPost]
        public JsonResult GetPostFull(int? id)
        {
            return null;

        }







        public ActionResult Logout()
        {
            Session[Config.UserLoginKey] = null;
            return RedirectToAction("Login");
        }



    }
}