﻿using StreetArt.Data.Models.Entities;
using StreetArt.Models;
using StreetArt.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;


namespace StreetArt.Controllers
{
    public class WorksController : Controller
    {
        // GET: Porfolios
        public ActionResult Index(string timeSort = "desc", string qsearch = "")
        {
            ViewBag.timeSort = timeSort;
            ViewBag.qsearch = qsearch;
            ViewBag.ViewName = "Index";
            ViewBag.addParam = "";

            var model = new WorkViewModelPr();
           

            using (EFDbContext _db = new EFDbContext())
            {
                IQueryable<WorkViewModel> md = _db.Works.Select(o => new WorkViewModel
                {
                    Id = o.Id,
                    PosterImage = o.PosterImage,
                    Approved = o.Approved,
                    Title = o.Title,
                    Favourite = o.Favourite,
                    User = o.User,
                    UploadedTime = o.UploadedTime,
                    Likes = o.Likes.Count(),
                    Shares = o.Shares.Count(),
                    Views = o.Views.Count(),
                }).Where(q => q.Approved == true);





                if (!string.IsNullOrEmpty(qsearch))
                    md = md.Where(q => q.Title.Contains(qsearch));

                if (timeSort == "desc")
                    md = md.OrderByDescending(q => q.UploadedTime);
                else
                    md = md.OrderBy(q => q.UploadedTime);

                model.UserLikedPosts = new List<int>();

                bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();
                if (UserLogedIn)
                {
                    var userModel = StreetArt.Helpers.AccountHelper.GetLoginedUser();
                    model.UserLikedPosts = _db.LikesToUsers.Where(q => q.UserId == userModel.Id).Select(q => q.WorkId).ToList();
                }
                model.Works = md.ToList();
                return View(model);

            }



        }

        public ActionResult User(string username, string timeSort = "desc", string qsearch = "")
        {
            try
            {
                ViewBag.timeSort = timeSort;
                ViewBag.qsearch = qsearch;
                ViewBag.ViewName = "User";


                if (string.IsNullOrEmpty(username)) throw new Exception();

                using (EFDbContext _db = new EFDbContext())
                {
                    var user = _db.Users.FirstOrDefault(q => q.Username == username);
                    if (user == null) throw new Exception();


                    var mainModel = new WorkViewModelPr();


                    IQueryable<WorkViewModel> model = _db.Works.Select(o => new WorkViewModel
                    {
                        Id = o.Id,
                        PosterImage = o.PosterImage,
                        Likes = o.Likes.Count(),
                        Shares = o.Shares.Count(),
                        Views = o.Views.Count(),
                        UserId = o.UserId,
                        Approved = o.Approved,
                        Title = o.Title,
                        Favourite = o.Favourite,
                        User = o.User,
                        UploadedTime = o.UploadedTime
                    }).Where(q => q.Approved == true && q.UserId == user.Id);

                    if (!string.IsNullOrEmpty(qsearch))
                        model = model.Where(q => q.Title.Contains(qsearch));

                    if (timeSort == "desc")
                        model = model.OrderByDescending(q => q.UploadedTime);
                    else
                        model = model.OrderBy(q => q.UploadedTime);

                    var resp = model.ToList();

                    if (resp.Count == 0) throw new Exception();

                    ViewBag.addParam = "&username=" + user.Username;




                    mainModel.UserLikedPosts = new List<int>();

                    bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();
                    if (UserLogedIn)
                    {
                        var userModel = StreetArt.Helpers.AccountHelper.GetLoginedUser();
                        mainModel.UserLikedPosts = _db.LikesToUsers.Where(q => q.UserId == userModel.Id).Select(q => q.WorkId).ToList();
                    }
                    mainModel.Works = model.ToList();

                    return View("index", mainModel);

                }
            }
            catch (Exception)
            {
                return RedirectToAction("index");
            }
        }




        [HttpPost]
        public ActionResult LikePost(int? id)
        {
            try
            {
                if (id == null) throw new Exception();

                int postId = int.Parse(id.ToString());

                int likesCount;

                using (EFDbContext _db = new EFDbContext())
                {
                    var post = _db.Works.FirstOrDefault(q => q.Id == id);
                    if (post == null) throw new Exception();

                    bool UserLogedIn = Core.Helpers.AuthHelper.UserLoggedIn();

                    if (!UserLogedIn) throw new Exception("notSignedIn");

                    var userModel = Helpers.AccountHelper.GetLoginedUser();

                    var likeToUser = _db.LikesToUsers.FirstOrDefault(q => q.UserId == userModel.Id && q.WorkId == post.Id);

                    if (likeToUser == null)
                    {
                        _db.LikesToUsers.Add(new LikeToUser
                        {
                            DateTime = DateTime.Now,
                            UserId = userModel.Id,
                            WorkId = postId
                        });
                    }
                    else
                    {
                        _db.LikesToUsers.Remove(likeToUser);
                    }



                    _db.SaveChanges();



                    likesCount = _db.LikesToUsers.Where(s => s.WorkId == postId).Count();

                }

                return Json(new
                {
                    Error = false,
                    Count = likesCount
                });
            }
            catch (Exception ex)
            {
                string errorMessage = "დაფიქსირდა შეცდომა";

                if (ex.Message == "notSignedIn")
                {
                    errorMessage = "გთხოვთ გაიაროთ ავტორიზაცია";
                }
                return Json(new
                {
                    Error = true,
                    Message = errorMessage
                });
            }

        }



        [HttpPost]
        public ActionResult SharePost(int? id)
        {
            try
            {
                if (id == null) throw new Exception();
                int postId = int.Parse(id.ToString());
               

                using (EFDbContext _db = new EFDbContext())
                {
                    var post = _db.Works.FirstOrDefault(q => q.Id == id);
                    if (post == null) throw new Exception();


                    _db.SharesToUsers.Add(new ShareToUser
                    {
                        DateTime = DateTime.Now,
                        WorkId = postId
                    });


                    _db.SaveChanges();
                    int countShares = _db.SharesToUsers.Where(s => s.WorkId == postId).Count();



                    return Json(new
                    {
                        Error = false,
                        Count = countShares
                    });

                }



               
            }
            catch (Exception ex)
            {
                string errorMessage = "დაფიქსირდა შეცდომა";

                if (ex.Message == "notSignedIn")
                {
                    errorMessage = "გთხოვთ გაიაროთ ავტორიზაცია";
                }
                return Json(new
                {
                    Error = true,
                    Message = errorMessage
                });
            }
        }


       
      

        [HttpPost]
        public ActionResult ViewPost(int? id)
        {

            try
            {
                if (id == null) throw new Exception();

                int postId = int.Parse(id.ToString());
               


                //ლოგიკა შესაცვლელია!!


                using (EFDbContext _db = new EFDbContext())
                {
                    var post = _db.Works.FirstOrDefault(q => q.Id == id);
                    if (post == null) throw new Exception();
                    
                   
                 


                    _db.ViewsToUsers.Add(new ViewToUser
                    {
                        DateTime = DateTime.Now,
                        WorkId = postId
                    });

                    _db.SaveChanges();

                    int countViews = _db.ViewsToUsers.Where(v => v.WorkId == post.Id).Count();

                    var userPosts = _db.Works.Where(q => q.UserId == post.UserId);

               

                    int UserPostLikes = userPosts.Select(q => q.Likes.Count).Sum();
                    int UserPostViews = userPosts.Select(q => q.Views.Count).Sum();
                    int UserPostShares = userPosts.Select(q => q.Shares.Count).Sum();

                    var cat = _db.SubCategories.Select(o => new
                    {
                        Id = o.Id,
                        Name = o.Name,
                        ParentName = o.Category.Name
                    }).FirstOrDefault(q=>q.Id == post.SubCategoryId);

                    var tags = _db.WorkToTags.Select(o => new
                    {
                        workId = o.WorkId,
                        tagName = o.Tag.Name
                    }).Where(q => q.workId == post.Id).ToList();

                    string tagList = "";

                    if(tags.Count != 0)
                    {
                        tagList = string.Join(" ", tags.Select(q => q.tagName).ToArray());
                    }

                     

                    return Json(new
                    {
                        Error = false,
                        Count = countViews,
                        Description = post.Description,
                        Category = cat,
                        Tags = tagList,
                        userId = post.UserId,
                        UserPostLikes = UserPostLikes,
                        UserPostViews = UserPostViews,
                        UserPostShares = UserPostShares
                    });

                }



               
            }
            catch (Exception ex)
            {
                string errorMessage = "დაფიქსირდა შეცდომა";
                return Json(new
                {
                    Error = true,
                    Message = errorMessage
                });
            }
        }


    }
}