namespace StreetArt.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<StreetArt.Models.EFDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(StreetArt.Models.EFDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //context.AdminUsers.AddOrUpdate(
            //  p => p.Username,
            //  new Data.Models.Entities.AdminUser { MainAdmin = true, Username= "admin", Password= "admin" }

            //);
            context.AdminUsers.AddOrUpdate(
                p => p.Id,
                new Data.Models.Entities.AdminUser { Id = 1,MainAdmin= true,Username="admin",Password="admin"}
            );
        }
    }
}
