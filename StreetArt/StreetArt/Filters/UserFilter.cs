﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace StreetArt.Filters
{
    public class UserFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (!Core.Helpers.AuthHelper.UserLoggedIn())
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                {"Controller", "Home"},
                {"Action", "Index"}
                });
            }
            base.OnActionExecuting(filterContext);
        }
    }
}