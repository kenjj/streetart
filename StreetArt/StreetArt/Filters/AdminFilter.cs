﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace StreetArt.Filters
{
  
   
    public class AdminFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (!Core.Helpers.AuthHelper.AdminLoggedIn)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                {"Controller", "Admin"},
                {"Action", "Index"}
                });
            }
            base.OnActionExecuting(filterContext);
        }
    }
}