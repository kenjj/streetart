﻿$(document).ready(function () {

    $("#register-btn").on('click', function () {
        var firstCheckbox = $("input[name=ConfirmProducts]:checked").length;
        var secondCheckbox = $("input[name=ConfirmTerms]:checked").length;

        if (firstCheckbox == 0 || secondCheckbox == 0) {
            $(".error.confirmation-error").css("display", "block");
            $("#register-form").bind("submit", function (e) {
                event.preventDefault();
            })
        }
        else {
            $(".error.confirmation-error").css("display", "none");
            $('#register-form').submit();
        }
    });


    $(".avatar-img").on("click", function () {
        $(this).next().toggleClass("active");
    });

    $("body").click( function (e) {
          try {
              if (e.target.className !== "dropdown" && e.target.className !== "avatar-img") {
                  $(".dropdown").removeClass("active");
              }
          } catch (e) {
          }
      }
    );


    






    $(".confirmation").on("click", function () {
        console.log("comes in click");
        var firstCheckbox = $("input[name=ConfirmProducts]:checked").length;
        var secondCheckbox = $("input[name=ConfirmTerms]:checked").length;

        if (firstCheckbox != 0 && secondCheckbox != 0) {
            $(".error.confirmation-error").css("display", "none");
        }
    });



    $("#Photo").change(function () {
        $("#image-form").submit();
    });





    // Like Logic
    $(".social.like").on("click", function () {
        var action = $(this).attr("data-action");
        var id = parseInt($(this).attr("data-id"));
        var domObject = $(this);

        $.ajax({
            url: U + "Works/" + action,
            type: 'POST',
            data: {
                'id': id
            },
            dataType: 'json',
            success: function (data) {

                if (data.Error) {
                    swal(data.Message, "", "error")
                }
                else {
                    console.log();
                    domObject.hasClass("active") ? domObject.removeClass("active") : domObject.addClass("active");
                    domObject.find("p").html(data.Count);
                }

            },
            error: function (request, error) {
                console.log("comes in here Error!", "success");
               // alert("Request: " + JSON.stringify(request));
            }
        });
    });



    // View Logic
    $(".viewItem").on("click", function () {
        var action = $(this).attr("data-action");
        var id = parseInt($(this).attr("data-id"));
        var domObject = $(this).parent().find(".itemview");
        var workData = $(this).parent();
        $(".loading").css("display", "block")
        $(".bootstrap-wrapper").find(".modal").css("opacity", "0");
        $.ajax({
            url: U + "Works/" + action,
            type: 'POST',
            data: {
                'id': id
            },
            dataType: 'json',
            success: function (data) {
                $(".loading").css("display", "none")
                $(".bootstrap-wrapper").find(".modal").css("opacity", "1");

                if (data.Error) {
                    swal(data.Message, "", "error")
                }
                else {
                    UpdateWorkData(workData, data.Description, data.UserPostLikes, data.UserPostShares, data.UserPostViews, data.Category.ParentName, data.Category.Name, data.Tags, data.userId);
                   
                    domObject.hasClass("active") ? domObject.removeClass("active") : domObject.addClass("active");
                    domObject.find("p").html(data.Count);

                }
            },
            error: function (request, error) {
                console.log("comes in here Error!", "success");
               // alert("Request: " + JSON.stringify(request));
            }
        });
    });




    //Share Logic
    $(".social.share").on("click", function () {

    });

    $(document).on('click', '.share', function () {
        var src = $(this).parent().parent().parent().find('.workImage')[0].src;
        var title = $(this).parent().parent().parent().find('.workAuthor').val();
        var id = parseInt($(this).attr("data-id"));
        var domObject = $(this);
        console.log(src);
        console.log("title", title);
        FB.ui({
            method: 'feed',
            picture: src,
            name: "StreetArt",
            caption: title,
        }, function (response) {
            if (response && response.post_id) {
                console.log("responce", response);
                $.ajax({
                    url: U + "Works/SharePost",
                    type: 'POST',
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.Error) {
                            swal(data.Message, "", "error")
                        }
                        else {
                            console.log();
                            domObject.hasClass("active") ? domObject.removeClass("active") : domObject.addClass("active");
                            domObject.find("p").html(data.Count);
                        }
                    },
                    error: function (request, error) {
                        console.log("comes in here Error!");
                    }
                });
            }
            else {
                console.log(response)
            }
        });
    });


    $('.gamocera').click(function () {
        var userId = $(this).attr('data-userid');

        $.post(U + "Account/Follow", { userId: userId })
            .done(function (data) {
                if (data.error == true) {
                    swal("დაფიქსირდა შეცდომა", "გაიარეთ ავტორიზაცია / უკვე გამოწერილი გყავთ იუზერი", "error")
                } else {
                    swal("თქვენ გამოიწერეთ არტისტი!", "", "success")
                }
            });
    });
});




// Popup Data Change Logic
function UpdateWorkData(workData, description, likes, shares, views, categoryName, subCategoryName, tagsName, userId) {
    var mainData = $('.WorkData');
    console.log(workData);
    mainData.find(".worklTitle").html($(workData).find(".worklTitle").val());
    mainData.find(".workDescription").html(description);
    mainData.find(".workAuthor").html($(workData).find(".workAuthor").val());
    mainData.find(".workPublishDate").html($(workData).find(".workPublishDate").val());
    mainData.find(".workAuthorAvatar").attr("src", U + "images/userPhotos/" + $(workData).find(".workAuthorAvatar").val());
    mainData.find(".prodcat").html(categoryName + ' / '+ subCategoryName +' / ' + tagsName);

    mainData.find(".gamocera").attr('data-userid', userId);

    mainData.find(".message-to-user").find('a').attr("href", "chat" + "/" + userId);


    mainData.find(".workImage").attr("src", $(workData).find(".workImage").attr("src"));
    mainData.find(".workImageData").attr("data-src", $(workData).find(".workImage").attr("src"));
    mainData.find(".workImageData").attr("data-sub-html", $(workData).find(".worklTitle").val());



    mainData.find(".workLikes").html($(workData).find(".workLikes").html());
    mainData.find(".workViews").html($(workData).find(".workViews").html());
    //console.log("სადაც უნდა დაემატოს", mainData.find(".workViews").html());
    //console.log("რაც უნდა დაემატოს", $(workData).find(".workViews").html());

   

    mainData.find(".workAuthorViews").html(views);
    mainData.find(".workAuthorLikes").html(likes);
    mainData.find(".workAuthorShares").html(shares);

    

    mainData.find(".workViews").html($(workData).find(".workViews").html());
}


function beginChatLoading()
{
    $(".loading.inchat").css("display", "block");
    $("textarea").attr("disabled", "disabled");
    document.getElementById('users').style.pointerEvents = 'none'; // Disable
}

function endChatLoading()
{
    $(".loading.inchat").css("display", "none");
    $("textarea").removeAttr('disabled');
    document.getElementById('users').style.pointerEvents = 'auto'; // Enable
}



function CheckUnreadMessages(index)
{
    //$.post(U + "Chat/UnreadMessages", { id: index })
    //             .done(function (data) {
    //                 console.log("data", data);
    //                 if (data.error == true)
    //                 {
    //                     console.log("Something wrong here");
    //                 }
    //                 else
    //                 {
    //                     alert(data.Count);
    //                 }
    //             });

    $.ajax({
        url: U + "Chat/UnreadMessages",
        type: 'POST',
        data: {
            'id': index
        },
        dataType: 'json',
        success: function (data) {
            if (data.Error) {
                console.log("Can't load uneread messages data!");
            }
            else {
                if (data.Count > 0)
                {
                    $(".message-icon.logined").addClass("alert").attr("data-notcount", data.Count)
                }
                else
                {
                    $(".message-icon.logined").removeClass("alert");
                }
            }

        },
        error: function (request, error) {
            console.log("Can't load uneread messages data!");
        }
    });


}