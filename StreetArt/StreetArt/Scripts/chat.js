﻿$(function () {
    var smileyMap = {
        "Emoticons/1.png": [":)", ":-)"],
        "Emoticons/2.png": [":(", ":-("],
        "Emoticons/3.png": [":/", ":-/"],
        "Emoticons/4.png": [":*", ":-*"],
        "Emoticons/5.png": [":@", ":-@"],
        "Emoticons/6.png": ["b-)", "B-)", "b-)", "B-)"],
        "Emoticons/7.png": ["<3", "&lt;3"],
        "Emoticons/8.png": [":p", ":-p", ":P", ":-P"],
        "Emoticons/9.png": [";)", ";-)"],
        "Emoticons/10.png": [":d", ":-d", ":D", ":-D"]
    };

    var insertSmiley = function (basePath, smileys) {
        var replacements = [];
        Object.keys(smileys).forEach(function (file) {
            var _file = "<img class=\"emotion\" src=\"" + basePath + file + "\"\>";
            smileys[file].forEach(function (chars) {
                var reg = new RegExp(chars.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), "g");
                replacements.push([reg, _file]);
            });
        });
        return function (text) {
            return replacements.reduce(function (line, replacement) {
                return line.replace(replacement[0], replacement[1]);
            }, text);
        }
    }(U + "images/", smileyMap);

    currentUserFullName = undefined;
    currentUserMessaging = undefined;

    var lastMessageSeen = false;
    var chat = $.connection.chatHub;
    var serverStarted = false;
    

    initNewUser($('.chat-users-cont').eq(0));

    $.connection.hub.start().done(function () {
        serverStarted = true;
        chat.server.connect(token);

    });
        

    $.connection.chatHub.client.connected = function () {
        alert("conn");
    };

    $.connection.chatHub.client.disconnected = function () {
        alert("disc");
    };


    $('#send-message').click(function (e) {

        e.preventDefault();
        var $text = $('#text-input').val();
        if (!$text) return;
        chat.server.sendMessage(currentUserMessaging, $text);
        addMessageHtml('from', $text);
        $('#text-input').val('').focus();
    });
    $(document).keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            if($('#text-input').hasClass('focus'))
                $('#send-message').click();
        }
    });
    $('#users').on('click', '.chat-users-cont', function () {
        initNewUser($(this));
    });

    $('#text-input')
        .focus(function () {
            if (!$(this).hasClass('focus')) $(this).addClass('focus');
            seen(currentUserMessaging);
        })
        .blur(function () {
            $(this).removeClass('focus');
        })

    $.connection.chatHub.client.newMessage = function (fromUserId, toUserId, content) {
        newMessage(fromUserId, toUserId, content);
    };

    $.connection.chatHub.client.newPersonMessage = function (person, content) {
        newPersonMessage(person, content);
    };
 
    function initNewUser(user) {

        if ($('.chat-users-cont').length <=1) {
            if (user.hasClass('not-message')) return;
        }
       
        if (user.hasClass('active')) return;

        $('.chat-users-cont').removeClass('active');

        var id =user.attr('data-userid');
        initNewMessageUser(id);
       
       
        currentUserFullName = user.find('.userName').text();
        currentUserMessaging = id;

        user.addClass('active');
        
        
    }

    function initNewMessageUser(id) {
        $('.messages-container').html('');
        beginChatLoading()

        $.post(U + "Chat/History", { id: id }, function (data) {

            if (data.Error == false) {


                $.each(data.Messages, function () {

                    var cls = "to"
                    if (!this.me) {
                        cls = "from";
                    }
                    addMessageHtml(cls, this.Content);

                });
                endChatLoading();
                $('#text-input').focus().val('');
            } else
                swal("Opss...", "Some Error", "error");


        });
    }
    $(".message-count").bind("DOMSubtreeModified", function () {
        console.log(this);
    });
    function getUserDivById(userid) {
        return $('.chat-users-cont[data-userid="' + userid + '"]');
    }
    function newMessage(fromUserId, toUserId, content) {
        $('.chat-users-cont.not-message').remove();
        lastMessageSeen = true;
        var $user = getUserDivById(fromUserId);

        if ($user.length == 0) {
            var dataQ = { id: fromUserId };

            $.post(U + "Chat/getUser", dataQ, function (data) {

                if (data.Error == false) {

                    newPersonMessage(data.User);


                } else
                    swal("Opss...", "Some Error", "error");


            });


        } else {

            if (currentUserMessaging == fromUserId) {
                addMessageHtml('to', content);
            } else {
                var messageContainer = $user.find('.message-count').css("opacity","1");
                var messageCount = parseInt(messageContainer.html());
                messageCount++;
                messageContainer.text(messageCount);
            }

        }



    }

    function seen(userId) {


        if (serverStarted) {
            chat.server.seen(userId);
        } else {
            setTimeout(function () {
                seen(userId)
            },500);
        }

        lastMessageSeen = true;
        var $user = getUserDivById(userId);
        $user.find('.message-count').html('0').css("opacity","0");
        
        
    }


    function newPersonMessage(person) {
        console.log(person);
        var html = '<div class="chat-users-cont"  data-userid="' + person.Id + '">' +
                        '<div class="chat-users">' +
                            '<div class="chat-user-img" style="background-image:url(../../images/UserPhotos/' + person.Avatar + ')"></div>' +
                            '<div class="user-info">' +
                                '<h3 class="userName">' + person.FirstName + " " + person.LastName + '</h3>' +
                                '<p>' + person.Date + '</p>' +
                            '</div>' +
                            '<span class="message-count">' + person.MessageCount + '</span>' +
                        '</div>' +
                    '</div>';
        $('#users').append(html);
    }

    function addMessageHtml(dir, content) {

        var str = getMessageContainer(dir, content);

        $('.messages-container').append(str);

        var objDiv = $('.messages-container');
        if (objDiv.length > 0) {
            objDiv[0].scrollTop = objDiv[0].scrollHeight;
        }


    }



    function getMessageContainer(addtClass, content) {
        var name = addtClass == "from" ? "Me" : currentUserFullName;
        return '<div class="message ' + addtClass + '">' +
                    '<h3>' + name + '</h3>' +
                    '<p>' + insertSmiley(html_escape_string(content)) + '</p>' +
                '</div>';
    }


    function html_escape_string(str) {
        return str.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }

   
});

