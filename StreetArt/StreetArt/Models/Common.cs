﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Models
{
    public static class Common
    {
        public static List<string> Month = new List<string>()
        {
            "იანვარი", "თებერვალი","მარტი", "აპრილი","მაისი", "ივნისი","ივლისი", "აგვისტო","სექტემბერი", "ოქტომბერი","ნოემბერი", "დეკემბერი"
        };
    }
}