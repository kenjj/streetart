﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace StreetArt.Models
{
    public class EFDbContext : DbContext
    {
        public DbSet<StreetArt.Data.Models.Entities.AdminUser> AdminUsers { get; set; }
        public DbSet<StreetArt.Data.Models.Entities.User> Users { get; set; }

        public EFDbContext() : base("DefaultConnection")
        {
            
        }
        public DbSet<StreetArt.Data.Models.Entities.Message> Messages { get; set; }
        public DbSet<StreetArt.Data.Models.Entities.Event> Events { get; set; }

        public DbSet<StreetArt.Data.Models.Entities.Slider> Sliders { get; set; }

        public DbSet<StreetArt.Data.Models.Entities.Work> Works { get; set; }
        public DbSet<StreetArt.Data.Models.Entities.MainPhoto> MainPhotoes { get; set; }

        public DbSet<StreetArt.Data.Models.Entities.SuggestedEvent> SuggestedEvents { get; set; }

        public DbSet<StreetArt.Data.Models.Entities.LikeToUser> LikesToUsers { get; set; }

        public DbSet<StreetArt.Data.Models.Entities.ViewToUser> ViewsToUsers { get; set; }

        public DbSet<StreetArt.Data.Models.Entities.ShareToUser> SharesToUsers { get; set; }

        public DbSet<StreetArt.Data.Models.Entities.Tag> Tags { get; set; }

        public DbSet<StreetArt.Data.Models.Entities.Category> Categories { get; set; }

        public DbSet<StreetArt.Data.Models.Entities.SubCategory> SubCategories { get; set; }
        public DbSet<StreetArt.Data.Models.Entities.WorkToTag> WorkToTags { get; set; }
        public DbSet<StreetArt.Data.Models.Entities.Follow> Follows { get; set; }

        public System.Data.Entity.DbSet<StreetArt.Data.Models.Entities.SellContent> SellContents { get; set; }
    }
}