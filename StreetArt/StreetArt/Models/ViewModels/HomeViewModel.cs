﻿using StreetArt.Data.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Models.ViewModels
{
    public class HomeViewModel
    {
        public List<Event> Events { get; set; }
        public List<LngLat> Markers { get; set; }
        public List<Slider> Sliders { get; set; }
        public List<WorkViewModel> FavouriteWorks { get; set; }
        public MainPhoto MainPhoto { get; set; }
        public List<int> UserLikedPosts { get; set; }
    }
}