﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StreetArt.Models.ViewModels
{
    public class PortFolioViewModel
    {
        public int Id { get; set; }

        [AllowHtml]
        [Required]
        public string Discription { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string PosterImage { get; set; }

        public HttpPostedFileBase defaultImage { get; set; }

        [Required]
        public string projectDescription { get; set; }
        public List<int> tags { get; set; }

        [Required]
        public int subcategory { get; set; }

    }
}