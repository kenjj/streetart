﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StreetArt.Models.ViewModels
{
    public class DetailsViewModel
    {
        public string AboutTitle { get; set; }
        public string AboutText { get; set; }

        public string FbLink { get; set; }
        public string TwLink { get; set; }
        public string DribbleLink { get; set; }

        public string LinkdinLink { get; set; }

        public string VimeoLink { get; set; }

    }
}