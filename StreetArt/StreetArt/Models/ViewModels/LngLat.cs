﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Models.ViewModels
{
    public class LngLat
    {
        public string Lng { get; set; }
        public string Lat { get; set; }
    }
}