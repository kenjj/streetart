﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StreetArt.Models.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "გთხოვთ ჩაწეროთ სახელი")]
        public string FirstName { get; set; }


        [Required(ErrorMessage = "გთხოვთ ჩაწეროთ გვარი")]
        public string LastName { get; set; }


        [RegularExpression(@"^\S*$", ErrorMessage = "მომხმარებლის სახელი არ უნდა შეიცავდეს ცარიელ ადგილს")]
        [Required(ErrorMessage = "გთხოვთ ჩაწეროთ მომხმარებლი სახელი")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "გთხოვთ ჩაწეროთ ელ.ფოსტა")]
        [EmailAddress(ErrorMessage = "გთხოვთ ელ.ფოსტა ჩაწეროთ სტანდარტუი სახით")]
        public string Email { get; set; }


        [Required(ErrorMessage = "გთხოვთ ჩაწეროთ პაროლი")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "პაროლი უნდა შედგებოდეს მინიმუმ 6 სიმბოლოსგან")]
        public string Password { get; set; }


        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "პაროლები არ ემთხვევა")]
        public string ConfirmPassword { get; set; }



        public string City { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}