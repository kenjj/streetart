﻿using StreetArt.Data.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Models.ViewModels
{
	public class WorkViewModelPr
	{
        public List<WorkViewModel> Works { get; set; }
        public List<int> UserLikedPosts { get; set; }
        public User UserProfile { get; set; }
    }

    public class WorkViewModel
    {
        public int Id { get; set; }


        public int UserId { get; set; }
        public User User { get; set; }


        public int Likes { get; set; }
        public int Shares { get; set; }
        public int Views { get; set; }


        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime UploadedTime { get; set; }

        public string PosterImage { get; set; }
        public bool Approved { get; set; }
        public bool Favourite { get; set; }

        public string defaultImage { get; set; }

        public bool Isliked { get; set; }

    }
}