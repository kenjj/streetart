﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Core.Models
{
    public static class Config
    {
        public static string AdminLoginKey = "AdminLogedIn";
        public static string UserLoginKey = "UserLogedIn";
        public static string MainAdminLoginKey = "MainAdminLogedIn";
    }
}