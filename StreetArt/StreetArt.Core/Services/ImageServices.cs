﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace StreetArt.Core.Services
{
    public static class ImageServices
    {
        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        public static bool saveImage(HttpPostedFileBase image, string Path)
        {
            try
            {


                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {
                    file.Delete();
                }
                image.SaveAs(Path);


                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }
    }
}