﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Core.Services
{
    public class CryptoService
    {
        private static string secretKey
        {
            get
            {
                return "078l31hWh6wxJXMdV4C6r54dB79MdNLAOYg5MbeJ";
            }
        }
        public static string Encode(Dictionary<string, object> data)
        {
            
          
            return JWT.JsonWebToken.Encode(data, secretKey, JWT.JwtHashAlgorithm.HS256);
        }
        public static string Decode(string token)
        {
           
            try
            {
                return JWT.JsonWebToken.Decode(token, secretKey);
                
            }
            catch (JWT.SignatureVerificationException)
            {
               
                return "error";
            }
           
        }
    }
}