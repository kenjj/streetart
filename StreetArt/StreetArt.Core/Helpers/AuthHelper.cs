﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreetArt.Core.Helpers
{
    public static class AuthHelper
    {
        public static bool IsMainAdmin {
            get{
                return HttpContext.Current.Session[Models.Config.MainAdminLoginKey] == null ? false : true;
            }
            private set {}
        }
        public static bool AdminLoggedIn
        {
            get
            {
                return HttpContext.Current.Session[Models.Config.AdminLoginKey] == null ? false : true;
            }
            private set { }
        }
        public static bool UserLoggedIn()
        {
            try
            {
                return HttpContext.Current.Session[Models.Config.UserLoginKey] == null ? false : true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

    }
}